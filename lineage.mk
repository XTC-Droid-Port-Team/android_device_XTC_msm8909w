#
# Copyright (C) 2022 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

$(call inherit-product, device/XTC/msm8909w/full_msm8909w.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/cm/config/common_mini_phone.mk)

PRODUCT_NAME := lineage_msm8909w
BOARD_VENDOR := XTC

PRODUCT_BUILD_PROP_OVERRIDES += \
    BUILD_FINGERPRINT="XTC/msm8909w/msm8909w:7.1.1/NMF26F/root12180122:user/release-keys" \
    PRIVATE_BUILD_DESC="msm8909w-user 7.1.1 NMF26F eng.root.20191218.012241 release-keys"
